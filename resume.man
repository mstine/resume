.TH resume 7 "13 Mar 2023" "1.0" "Matt Stine's CV"
.SH NAME
resume \- Matt Stine's CV
.SH AUTHOR
Matt Stine
.br
4656 N. Terrace Stone Dr.
.br
Olive Branch, MS 38654
.br
.MT matt@mattstine.com
.ME
.SH SYNOPSIS
.B man
\[char46]/resume.man
.SH DESCRIPTION
Matt Stine is a 22-year veteran software engineer and architect, with eight of those years spent consulting for multiple Fortune 500 companies. He is the author of Migrating to Cloud-Native Application Architectures from O’Reilly, and has taught Domain-Driven Design and Event-Driven Architecture courses for O'Reilly's Learning Platform since 2018. Matt's software philosophy is built on a foundation of systems thinking, complexity theory, principled pragmatism, and rational tradeoff negotiation. He is currently an Executive Director and Senior Principal Software Engineer in JPMorgan Chase’s Chief Technology Office. Matt has spoken at conferences ranging from JavaOne to OSCON to YOW!, was a ten-year member of the No Fluff Just Stuff tour, and previously served as Technical Editor of NFJS the Magazine. In his spare time, Matt can be found hacking games and applications in MOS 6502 Assembly Language for his Commodore 64 and breadboarding circuits for his next homebrew electronics build.
.SH SKILLS
\fBLanguages\fP
.RS 4
Extensive professional experience with Java, Kotlin, JavaScript, and Go.
.br
Other work and personal project experience with Rust, TypeScript, Python,
C#, C/C++, Clojure, Groovy, Ruby.
.RE
.sp
\fBTechnologies\fP
.RS 4
Spring, Spring Boot, Maven, Gradle, PicoCli (JVM), Cobra CLI (Go), GraphQL,
React, Node/NPM, Docker, Kubernetes, Jenkins, Cloud Foundry, Terraform,
OpenRewrite, Oracle, PostgreSQL, MySQL/MariaDB,
Structurizr, IntelliJ IDEA, VSCode, Vim/Neovim, 
Bitbucket, GitHub, JIRA, Pivotal Tracker.
.RE
.sp
\fBCloud\fP
.RS 4
Primary professional experience with AWS, including: IAM, VPC, EC2, S3, EKS, ECS, Lambda, Route53.
.br
Other work and personal project experience with Google Cloud, Microsoft Azure, DigitalOcean.
.RE
.sp
\fBFundamentals\fP
.RS 4
Network & Web, Object-Oriented Design, Functional
Programming, REST, Test-Driven Development, Continuous Delivery,
Systems Architecture, Enterprise Architecture,
Extreme Programming, Lean, Kanban, Systems Thinking.
.RE
.sp
\fBCertifications\fP
.RS 4
Hashicorp Certified Terraform Associate
.br
ID#: d0d34c25-25a1-4584-b596-6526434f9616
.br
Expiration: 2025-01-29
.RE
.SH EXPERIENCE
\fBJPMorgan Chase & Co.\fP
.br
\fBExecutive Director - Senior Principal Software Engineer\fP
.br
\fBChief Technology Office\fP
.br
September 2020\(en
.IP \(bu 2
Led a cross-LOB team of programming language leaders and stakeholders to actively manage the firmwide programming language ecosystem.
.IP \(bu 2
Delivered a GraphQL-based API providing firmwide source repo-level usage statistics for languages, frameworks, container images, Jenkins builder nodes, etc., which is currently used to drive dependency hygiene and modernization initatives.
.IP \(bu 2
Led the design and implementation of an automated application hygiene capability using OpenRewrite. Co-authored recipes to migrate applications to the most recent recommended versions of internal frameworks, and delivered a CLI allowing for easy recipe execution beyond the context of a Maven build environment.
.IP \(bu 2
Created a Structurizr DSL-based automation toolchain for generating static websites containing C4 architecture models, lightweight architecture decision records (ADRs), and supplementary architecture documentation from source documents federated to the application source code repositories.
.IP \(bu 2
Onboarded Rust as a firm-supported language for software development.
.IP \(bu 2
Delivered support for publication of internally developed IntelliJ IDEA plugins through the firm plugin respository.
.PP
\fBJPMorgan Chase & Co.\fP
.br
\fBExecutive Director - Senior Principal Architect\fP
.br
\fBAsset & Wealth Management\fP
.br
June 2019\(enSeptember 2020
.IP \(bu 2
Conducted a systematic review of all user-facing and back-end service components for the primary Wealth Management operations platform. Prepared findings and recommendations for each component/team, and guided teams on implementation as needed.
.IP \(bu 2
Product owner for an experimental web-based WYSIWYG C4 architecture model repository.
.IP \(bu 2
Modernized the LOB's architecture practice, including documentation standards, architecture review and approval processes, and architectural principles and guidelines.
.IP \(bu 2
Produced opinionated architecture guidance for database and caching technology selection/usage, secure automated schema migrations, and API design.
.PP
\fBPivotal Software, Inc.\fP
.br
\fBGlobal Chief Technology Officer\fP
.br
\fBSoftware Architecture\fP
.br
September 2017\(enJune 2019
.IP \(bu 2
My focus is on enabling IT executives, technical leadership, and senior architects in Fortune 500 companies to harness the benefits of cloud-native architecture through advisory relationships, architectural reviews, education sessions, and reference architecture initiatives. As a founding member of the Office of the CTO, I have helped to develop our team's charter, roles and responsibilities, as well as a common engagement model that we use with clients. I am currently engaged with multiple clients in the retail, healthcare, logistics, and financial services sectors across the Eastern United States and Europe.
.PP
\fBNo Fluff Just Stuff\fP
.br
\fBKeynote Speaker and Technical Trainer\fP
.br
April 2010\(enJune 2019
.IP \(bu 2
Spoke on topics including cloud native application architecture, advanced data architecture patterns, twelve-factor applications, Docker and Linux container orchestration platforms, application metrics/logging/monitoring, effective core Java programming, Spring, lean/agile software development, Kanban, software archaeology, object-oriented design principles, functional programming, OSGi, automated testing, Groovy/Grails, DevOps (Puppet/Chef/Vagrant), web development, and enterprise application integration.
.PP
\fBNFJS the Magazine\fP
.br
\fBTechnical Editor\fP
.br
December 2012\(enDecember 2017
.IP \(bu 2
Sourced, edited, and published 3-4 technical articles per month via NFJS the Magazine's (now defunct) website.
.IP \(bu 2
Developed and maintained an automated magazine production toolchain composed from GitHub and Asciidoctor.
.PP
\fBPivotal Software, Inc.\fP
.br
\fBProduct Owner, Spring Technology Portfolio\fP
.br
\fBStrategic Product Organization\fP
.br
May 2016\(enAugust 2017
.IP \(bu 2
Maintained an active feedback loop between the Spring and Cloud Foundry R&D organizations and customers developing cloud-native application architectures.
.IP \(bu 2
Supported firm-wide cloud-native/microservices reference architecture efforts across multiple Fortune 50 organizations.
.IP \(bu 2
Drove the creation of Pivotal reference architecture applications, documentation, and guidance.
.PP
\fBPivotal Software, Inc.\fP
.br
\fBSenior Product Manager\fP
.br
\fBSpring Cloud Services for Pivotal Cloud Foundry\fP
.br
December 2014\(enMay 2016
.IP \(bu 2
Product owner/manager for Spring Cloud Services for Pivotal Cloud Foundry (PCF), an enterprise-ready, turnkey distribution of Spring Cloud and Netflix OSS components designed to support the development and operation of cloud native/microservices architectures on PCF.
.IP \(bu 2
Customer adoption of Spring Cloud Services is directly traceable to the close of multiple large (>$1MM) software subscription purchases of PCF across the Fortune 500.
.IP \(bu 2
Supported outbound pre-sales and marketing activities for Spring Cloud Services to customers, including the creation and delivery of multiple 1-2 day, hands-on “Cloud Native Application Workshops.” 
.IP \(bu 2
Supported multiple analyst-facing (Redmonk, Gartner, etc.) and customer-facing webinars and technical briefings.
.IP \(bu 2
Lead a modified version of the Pivotal Labs agile software process with a 100% distributed team ranging from 4 engineers at inception to 10 engineers at scale, stretching from San Francisco, CA, to Southampton, U.K.
.IP \(bu 2
Authored detailed specifications for enterprise-ready security features for all Spring Cloud Services catalog services, including integration with PCF’s multi-tenant authentication and authorization service (UAA).
.IP \(bu 2
Authored detailed specifications for zero-downtime upgradability of all Spring Cloud Services catalog services.
.IP \(bu 2
Led the creation of the SteelToe project (https://steeltoe.io), with the goal of supporting development and operation of cloud native applications authored using Microsoft ASP.NET Core 1.0, and integrated with SCS.
.PP
\fBPivotal Software, Inc.\fP
.br
\fBPlatform Engineer\fP
.br
\fBCloud Foundry Engineering\fP
.br
June 2013\(enDecember 2014
.IP \(bu 2
Co-led the creation of Pivotal’s “Cloud Native Application” and microservices product and marketing strategies.
.IP \(bu 2
Drove technical pre-sales activities resulting in multiple large (>$1MM) software subscription purchases of Pivotal Cloud Foundry (PCF) across the Fortune 500.
.IP \(bu 2
Performed guided PCF installs, supported technical evaluation projects, ran internal customer hackathons, developed custom BOSH releases and Pivotal Ops Manager “tiles,” and assisted in application modernization/migration efforts.
.IP \(bu 2
Responsible for developing PCF content, training, and certification programs for the global Field Engineering (technical pre-sales) teams.
.IP \(bu 2
Co-developed and co-delivered the first “Pivotal Cloud Platform Roadshow” events, exposing PCF and related technologies to hundreds of customers in the U.S. and Europe.
.IP \(bu 2
Developed and orchestrated the first demonstration of all of the newly formed Pivotal’s software product family to the executive leadership team.
.IP \(bu 2
Developed technical demo/screencast videos for most of the early PCF marketing activities.
.IP \(bu 2
Committer to Cloud Foundry BOSH (http://bosh.io).
.PP
\fBVMware/Pivotal\fP
.br
\fBSenior Consultant\fP
.br
\fBvFabric Professional Services Organization\fP
.br
February 2012\(enJune 2013
.IP \(bu 2
Solution architect on large (>400K servers), multi-year effort for Fortune 50 insurance company to replace existing WebSphere-based customer facing web infrastructure and systems with VMware vSphere and vFabric-based solution.
.IP \(bu 2
Responsible for authoring multiple Puppet modules.
.IP \(bu 2
Delivered three-site, multi-active solution for MCollective using ActiveMQ HA clusters with a federated network of brokers. Authored dynamic Puppet modules using PuppetDB to discover new cluster members and remove decommissioned cluster members.
.IP \(bu 2
Subject matter expert for F5 Networks iControl API. Developed Java services to integrate with F5 iControl as part of self-service provisioning portal application. Also delivered query index service for F5 configuration utilizing Apache Lucene in order to provide a 10x response time improvement for queries.
.IP \(bu 2
Provided expertise around Spring application architecture best practices, JVM tuning, and vFabric tc Server tuning.
.IP \(bu 2
Authored web services and backend automation to provide automated provisioning of NFS exports on NAS controller systems.
.IP \(bu 2
Designed enhancements to systems management database for management of network profiles, IP addresses, and hostnames. Authored web services for automated provisioning of network configurations for newly provisioned virtual machines.
.IP \(bu 2
Authored proof-of-concept around integration of existing self-service provisioning portal with VMware vCloud Director (vCD) and vFabric RabbitMQ utilizing the vCloud API and vCD AMQP notifications.
.PP
\fBAutoZone\fP
.br
\fBTechnical Architect\fP
.br
June 2011\(enFebruary 2012
.IP \(bu 2
Lead architect for ZMS GOL project, responsible for delivering retail store management systems for new Brazilian stores, including point of sale (POS), inventory management, integration with tax engine, and integration with third-party accounting and logistics providers.
.IP \(bu 2
Collaborated with program and project managers to reorganize multiple projects centered around functional business units (e.g. merchandising, store operations, etc.) into a single project focused on the delivery of cross-functional business process flows (e.g. store inventory replenishment, distribution center returns, retail B2B transactions, etc.).
.IP \(bu 2
Technical subject matter expert focused on Brazilian retail taxation.
.IP \(bu 2
Primary analyst focused on delivery of store management solution compliant with Brazilian electronic invoicing legislation, or Nota Fiscal Eletrônica (NF-e).
.IP \(bu 2
Aided in the design of integration of retail store POS systems with Brazilian fiscal printers (EPSON) using NRF ARTS Unified POS standard.
.IP \(bu 2
Provided agile coaching to multiple project teams.
.IP \(bu 2
Lead several automation initiatives, including a large-scale multi-module project migration to Gradle, and continuous deployment of QA systems utilizing Jenkins and Capistrano.
.PP
\fBSt. Jude Children's Research Hospital\fP
.br
\fBGroup Leader\fP
.br
\fBResearch Application Development\fP
.br
May 2008\(enJune 2011
.IP \(bu 2
Led an eleven member cross-disciplinary team (business analysts, developers, testers) and five member offshore development team in India.
.IP \(bu 2
Interviewed and hired five software engineers within a one year period, as well as interviewing and selecting all five offshore developers.
.IP \(bu 2
Directed the development of version 2 of our enterprise shared resource management system (SRM), utilizing an entity-attribute-value (EAV) data model for enhanced configurability and rapid delivery of services, as well as a modular microkernel/plugin architecture using Spring Dynamic Modules/OSGi technologies. This system currently supports four shared resources and will eventually support approximately 25 different shared resource facilities offering a diverse array of services including whole-genome sequencing, expression and genotyping arrays, proteomics, cell and tissue imaging, and pathology services. My responsibilities include requirements analysis, project management and scheduling, architectural oversight, code reviews, and technology selection.
.IP \(bu 2
Directed a five-member team, including one business analyst, one graphics designer, and four offshore developers to enhance our TrakIT publication management system (see description below) by adding an advanced curation interface, integration with additional public databases, and a user interface facelift.
.IP \(bu 2
Directed a three-member team to develop a Mouse Colony Management system for tracking pedigrees and genotypes using Grails, PostgreSQL, and YUI.
.IP \(bu 2
Ensured fully automated, continuous QA and delivery of all applications utilizing tools such as Gradle, Jenkins CI, Capistrano, Liquibase, and Selenium.
.PP
\fBSt. Jude Children's Research Hospital\fP
.br
\fBSenior Software Engineer\fP
.br
\fBHartwell Center for Bioinformatics and Biotechnology\fP
.br
May 2001\(enMay 2008
.IP \(bu 2
Led a three-member team to develop TrakIT, a publication management system for St. Jude publications. TrakIT integrates with multiple public databases to assemble an exhaustive set of St. Jude publication data. It allows investigators to annotate these publications with shared resource usage, cancer center/multidisciplinary program affiliations, and research categories, as well as selecting which publications they would like to appear on their faculty bio page on http://www.stjude.org. This application reduced a several week grant renewal process to a point-and-click report generation lasting a few seconds.
.IP \(bu 2
Led a three-member team to migrate 684,000 shared resource service orders spanning thirteen laboratory services and 23 years from the Hartwell Center's (http://www.hartwellcenter.org) legacy database to the Shared Resource Management (SRM) database. During this project I developed mappings and algorithms for migration of the data from the legacy schema to the SRM schema. I also developed test cases and SQL queries to validate the migration. I maintained the project schedule, conducted weekly status update meetings, and oversaw the development of Oracle PL/SQL packages that implemented the mappings and algorithms.
.IP \(bu 2
Led a two-member team to develop a web-based scheduling system for shared laboratory instrumentation. This system allows laboratory managers to setup calendars (daily, weekly, and monthly) for each of their instruments and allows users to login to the system and book appointments. Laboratory managers may also setup records for each of their technicians and then rank them on each instrument. Users who request appointments for technician-assisted usage are assigned the highest ranked available technician. If no technician is available, the booking is denied. The system also bills users at a defined hourly rate through the SRM system.
.PP
.SH EDUCATION
\fBUniversity of Mississippi\fP
.br
1997\(en2001
.PP
Bachelor of Science in Computer Science
.br
Sally McDonnell-Barksdale Honors College Graduate
.br
2001 Outstanding Senior Student - Computer and Information Science Department
.PP
.SH SEE ALSO
\fBPersonal Website\fP
.RS 4
.UR https://mattstine.com
.UE
.RE
.sp
\fBMastodon\fP
.RS 4
.UR https://mastodon.sdf.org/@mstine
.UE
.RE
.sp
\fBGitHub\fP
.RS 4
.UR https://github.com/mstine
.UE
.RE
.sp
\fBGitLab\fP
.RS 4
.UR https://gitlab.com/mstine
.UE
.RE
.sp
\fBSourcehut\fP
.RS 4
.UR https://sr.ht/~mstine/
.UE
.RE
